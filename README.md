# ArchUnit by example

## How to build project
./gradlew build

## Resources
https://www.archunit.org/userguide/html/000_Index.html
https://blogs.oracle.com/javamagazine/post/unit-test-your-architecture-with-archunit