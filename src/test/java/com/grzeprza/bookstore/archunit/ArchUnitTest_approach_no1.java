package com.grzeprza.bookstore.archunit;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition;

@AnalyzeClasses(
        packages = "com.grzeprza.bookstore.customer",
        importOptions = {ImportOption.DoNotIncludeTests.class}
)
public class ArchUnitTest_approach_no1 {

    @ArchTest
    private final ArchRule configRule = Architectures.layeredArchitecture()
            .layer("web").definedBy("..controller..")
            .layer("service").definedBy("..service..")
            .layer("repository").definedBy("..repository..", "..model..")
            .whereLayer("web").mayNotBeAccessedByAnyLayer()
            .whereLayer("service").mayOnlyBeAccessedByLayers("web")
            .whereLayer("repository").mayOnlyBeAccessedByLayers("service");
}
