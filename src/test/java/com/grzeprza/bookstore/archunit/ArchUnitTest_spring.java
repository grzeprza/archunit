package com.grzeprza.bookstore.archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

class ArchUnitTest_spring {
    private static JavaClasses importedClasses;

    @BeforeAll
    public static void setUp() {
        importedClasses = new ClassFileImporter()
                .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
                .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_ARCHIVES)
                .importPackages("com.grzeprza.bookstore.renting");
    }

    @Test
    public void springFieldDependencyInjectionShouldNotBeUsed() {
        ArchRule rule = ArchRuleDefinition.noFields()
                .should().beAnnotatedWith(Autowired.class)
                .because("Field injection is evil");
        rule.check(importedClasses);
    }

    @Test
    public void springSingletonComponentsShouldOnlyHaveFinalFields() {
        ArchRule rule = ArchRuleDefinition.classes()
                .that().areAnnotatedWith(Service.class)
                .or().areAnnotatedWith(Component.class).and().areNotAnnotatedWith(ConfigurationProperties.class)
                .or().areAnnotatedWith(Controller.class)
                .or().areAnnotatedWith(RestController.class)
                .or().areAnnotatedWith(Repository.class)
                .should().haveOnlyFinalFields();
        rule.check(importedClasses);
    }

    @Test
    public void configurationClassesShouldBeAnnotatedWithConfigurationAnnotation() {
        ArchRule rule = ArchRuleDefinition.classes()
                .that().haveSimpleNameEndingWith("Configuration")
                .should().beAnnotatedWith(Configuration.class);
        rule.check(importedClasses);
    }

    @Test
    public void integrationTestShouldEndWithSuffixIntegrationTest(){
        JavaClasses classes = new ClassFileImporter()
                .importPackages("com.grzeprza.bookstore.renting");

        ArchRule rule = classes().that().areAnnotatedWith(SpringBootTest.class)
                .should().haveSimpleNameEndingWith("IntegrationTest");

        rule.check(classes);
    }
}
