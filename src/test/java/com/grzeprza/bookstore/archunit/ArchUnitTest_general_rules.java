package com.grzeprza.bookstore.archunit;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.library.GeneralCodingRules;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ArchUnitTest_general_rules {

    private static JavaClasses importedClasses;

    @BeforeAll
    public static void setUp() {
        importedClasses = new ClassFileImporter()
                .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
                .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_ARCHIVES)
                .importPackages("com.grzeprza.bookstore.renting");
    }

    @Test
    public void noClassesShouldUseStandardStreams() {
        ArchRule rule = ArchRuleDefinition.noClasses()
                .should(GeneralCodingRules.ACCESS_STANDARD_STREAMS);
        rule.check(importedClasses);
    }

    @Test
    public void noClassesShouldThrowGenericExceptions() {
        ArchRule rule = ArchRuleDefinition.noClasses()
                .should(GeneralCodingRules.THROW_GENERIC_EXCEPTIONS);
        rule.check(importedClasses);
    }

    @Test
    public void noClassesShouldUseStandardLogging() {
        ArchRule rule = ArchRuleDefinition.noClasses()
                .should(GeneralCodingRules.USE_JAVA_UTIL_LOGGING);
        rule.check(importedClasses);
    }
}
