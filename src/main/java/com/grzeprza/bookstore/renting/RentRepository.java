package com.grzeprza.bookstore.renting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface RentRepository extends JpaRepository<Rent, Long> {
}
