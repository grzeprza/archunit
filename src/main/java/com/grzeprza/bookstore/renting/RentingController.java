package com.grzeprza.bookstore.renting;

import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rent")
class RentingController {
    @Autowired
    RentingService rentingService;

    @Autowired
    RentRepository repo;

    @PostMapping
    public ResponseEntity<?> rent(@RequestBody Rent rent){
        rentingService.rent(rent.bookIsbn, rent.userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> getRentByUser(@PathVariable Long userId){
        var userRent = this.repo.findAll().stream().filter(rent -> rent.userId == userId).collect(Collectors.toList());
        return ResponseEntity.ok(userRent);
    }
}
