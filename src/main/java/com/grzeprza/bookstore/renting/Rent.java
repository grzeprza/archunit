package com.grzeprza.bookstore.renting;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;


@Entity
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Rent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String bookIsbn;

    Long userId;

    public Rent() {}

    public Rent(Long id, String bookIsbn, Long userId){
        this.id = id;
        this.bookIsbn = bookIsbn;
        this.userId = userId;
    }
}
