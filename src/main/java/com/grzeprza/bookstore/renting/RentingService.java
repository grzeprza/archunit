package com.grzeprza.bookstore.renting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class RentingService {

    Logger logger = Logger.getLogger("RentingService");

    @Autowired
    public RentRepository rentRepo;

    public void rent(String bookIsbn, Long userId){
        System.out.println("Rent a book for user");
        try {
            rentRepo.save(new Rent(null, bookIsbn, userId));
        } catch (Exception e){
            logger.info("Kozak");
            throw new RuntimeException("Nie dziala");
        }
    }
}
