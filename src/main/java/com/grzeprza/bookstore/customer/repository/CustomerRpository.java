package com.grzeprza.bookstore.customer.repository;

import com.grzeprza.bookstore.customer.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRpository extends JpaRepository<Customer, Long> {

}
