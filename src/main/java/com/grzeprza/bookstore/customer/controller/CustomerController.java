package com.grzeprza.bookstore.customer.controller;

import com.grzeprza.bookstore.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {
    private final CustomerService customerService;

    @GetMapping
    ResponseEntity<?> getCustomers(){
        return ResponseEntity.ok(this.customerService.listCustomers());
    }
}
