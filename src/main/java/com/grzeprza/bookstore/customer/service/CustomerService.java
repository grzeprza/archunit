package com.grzeprza.bookstore.customer.service;

import com.grzeprza.bookstore.customer.controller.CustomerController;
import com.grzeprza.bookstore.customer.model.Customer;
import com.grzeprza.bookstore.customer.repository.CustomerRpository;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRpository customerRpository;

    public List<Customer> listCustomers(){
        return customerRpository.findAll();
    }
}
