package com.grzeprza.bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArchunitApplication  {

	public static void main(String[] args) {
		SpringApplication.run(ArchunitApplication.class, args);
	}

}
