package com.grzeprza.bookstore.warehouse;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.Repository;

interface WarehouseRepository extends Repository<Book, Long> {
    Optional<BookProjection> findByIsbn(String isbn);
    List<BookProjection> findByAuthor(String author);
    Page<BookProjection> findAllBy(Pageable page);
}