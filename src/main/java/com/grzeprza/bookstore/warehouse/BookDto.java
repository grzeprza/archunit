package com.grzeprza.bookstore.warehouse;


import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
class BookDto {
    private static final String FIELD_UNKNOWN = "UNKNOWN";

    private final String isbn;
    private final String author;
    private final String title;

    public static BookDto emptyBookDto(String isbn){
        return new BookDto(isbn, FIELD_UNKNOWN, FIELD_UNKNOWN);
    }
}
