package com.grzeprza.bookstore.warehouse;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@RequiredArgsConstructor
public class WarehouseFacade{

    private final WarehouseRepository warehouseRepository;

    public List<BookDto> listBooksByAuthor(String author) {
        var books = warehouseRepository.findByAuthor(author);
        return books.stream().map(BookUtils::mapToBookDto).toList();
    }

    public BookDto findByIsbn(String isbn){
        var book = warehouseRepository.findByIsbn(isbn);
        return book.map(BookUtils::mapToBookDto).orElseGet(() -> BookDto.emptyBookDto(isbn));
    }

    public Page<BookDto> findByAll(int pageNr, int pageSize){
        Pageable page = PageRequest.of(pageNr, pageSize);
        return warehouseRepository.findAllBy(page).map(BookUtils::mapToBookDto);
    }
}
