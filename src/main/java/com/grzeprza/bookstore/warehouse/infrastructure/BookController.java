package com.grzeprza.bookstore.warehouse.infrastructure;

import com.grzeprza.bookstore.warehouse.WarehouseFacade;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
record BookController(WarehouseFacade warehouseFacade) {

    @GetMapping("/book")
    public ResponseEntity<?> getBookByIsbn(@RequestParam Optional<String> isbn, @RequestParam Optional<String> author){
        if(isbn.isPresent()){
            var bookByIsbn = warehouseFacade.findByIsbn(isbn.get());
            return ResponseEntity.ok(bookByIsbn);
        }

        if(author.isPresent()){
            var booksByAuthor = warehouseFacade.listBooksByAuthor(author.get());
            return ResponseEntity.ok(booksByAuthor);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @GetMapping("/books")
    public ResponseEntity<?> getBooks(@RequestParam int pageNr, @RequestParam int pageSize){
        return ResponseEntity.ok(warehouseFacade.findByAll(pageNr, pageSize));
    }
}
