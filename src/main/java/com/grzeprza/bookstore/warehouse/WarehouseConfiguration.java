package com.grzeprza.bookstore.warehouse;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class WarehouseConfiguration {

    @Bean
     WarehouseFacade getWarehouseService(WarehouseRepository warehouseRepository){
        return new WarehouseFacade(warehouseRepository);
    }
}
