package com.grzeprza.bookstore.warehouse;

import lombok.experimental.UtilityClass;

@UtilityClass
class BookUtils {
     BookDto mapToBookDto(BookProjection book){
        return new BookDto(book.getIsbn(), book.getAuthor(), book.getTitle());
    }
}
