package com.grzeprza.bookstore.warehouse;

interface BookProjection {
    String getIsbn();
    String getTitle();
    String getAuthor();
}
