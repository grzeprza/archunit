package com.grzeprza.bookstore.warehouse;

import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NaturalId;

@Entity
@RequiredArgsConstructor
@Data
class Book {
    @Id
    private Long id;
    @NaturalId
    private String isbn;
    private String author;
    private String title;
}
