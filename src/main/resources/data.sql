insert into customer (id, name) values (1, 'Beata');
insert into customer (id, name) values (2, 'Paulina');

insert into book(id, isbn, author, title) values (1, '1234','Walter Isaacson' ,'Steve Jobs');
insert into book(id, isbn, author, title) values (2, '2345','Steven Levy' ,'In the Plex: How Google Thinks, Works, and Shapes Our Lives');
insert into book(id, isbn, author, title) values (3, '3456','John Carreyrou' ,'Bad Blood: Secrets and Lies in a Silicon Valley Startup');
insert into book(id, isbn, author, title) values (4, '4567','Walter Isaacson' ,'Yet another my book');